create database semanaacad;

CREATE TABLE Tbl_Palestra (
id numeric(9, 0) NOT NULL,
idPalestrante numeric(9, 0) NOT NULL,
descricao varchar(255), 
duracao numeric(4, 0), 
entrada numeric(5, 2),
dataPalestra timestamp, 
imageCard oid, 
nomeImageCard varchar(40),
imageDetalhe oid,
nomeImageDetalhe varchar(40),
titulo varchar(30),
subtitulo varchar(30),
localPalestra varchar(50),
PRIMARY KEY (id)
);

CREATE TABLE Tbl_Palestrante (
id numeric(9, 0) NOT NULL,
biografia text, 
nomePalestrante varchar(40), 
imagemPalestrante oid,
nomeimagemPalestrante varchar(40),
PRIMARY KEY (id)
);
ALTER TABLE Tbl_Palestra ADD CONSTRAINT FKTbl_Palestrante FOREIGN KEY (idPalestrante) REFERENCES Tbl_Palestrante (id);


create sequence sequenciapalestrante;
alter table tbl_palestrante alter column id set default nextval('sequenciapalestrante');


create sequence sequenciapalestra;
alter table tbl_palestra alter column id set default nextval('sequenciapalestra');