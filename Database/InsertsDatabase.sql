//insira os inserts que você fez no banco
INSERT INTO public.tbl_palestrante (id, biografia, nomepalestrante, imagempalestrante) VALUES(1, 'Os campos de treinamento têm seus apoiadores e seus responsáveis. Algumas pessoas não entendem por que você deve gastar dinheiro no campo de treinamento quando puder. Os campos de treinamento têm seus apoiadores e detratores.', 'Charlie Barber', NULL);

INSERT INTO public.tbl_palestra (id, idpalestrante, descricao, duracao, entrada, datapalestra, imagecard, imagedetalhe, titulo, localpalestra) VALUES(1, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.', 60, 0.00, '2019-09-03 22:04:08.507', NULL, NULL, 'palestra cara', 'here');

alter table tbl_palestrante add column facebook varchar(255);
alter table tbl_palestrante add column twitter varchar(255);

alter table tbl_palestra alter column imagedetalhe type text;
alter table tbl_palestra alter column imagecard type text;

update tbl_palestra set imagecard = 'img/test.jpg', imagedetalhe = 'img/test.jpg';
