<html>
<head>
<title>Cadastrar Palestrante</title>
<meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″>
<link rel="icon" href="https://www.unoesc.edu.br/favicon.ico" type="image/png">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="panel panel-primary">
		<h1>Cadastro de Palestrante</h1>
		<div class="panel-body">
			<form action="Palestrante.php" method="POST"
				enctype="multipart/form-data">
				Nome: <input type="text" name="nome" /><br> Bibliografia<br>
				<textarea name="biblio" rows="10" cols="60" maxlength="400"></textarea>
				<br />
				<br /> Imagem: <input type="file" name="image" /><br> <br />
				<input type="submit" name="submit" value="Salvar" /><br>
			</form>
		</div>

	</div>



	<hr>

	<div class="panel panel-primary">
		<h1>Cadastro de Palestra</h1>
		<div class="panel-body">
			<form action="Palestra.php" method="POST"
				enctype="multipart/form-data">
				ID Palestrante: <input type="number" name="idPalestrante"><br>
				Descrição: <br>
				<textarea name="descricao" rows="10" cols="60" maxlength="1000"></textarea>
				<br> <br> Duração: <input type="number" name="duracao"><br> Valor
				Entrada: <input type="number" name="entrada"><br>
				<br />
				<br /> Data: <input type="date" name="data"><br>
				<br /> <br />Imagem Card: <input type="file" name="imageCard" /><br>
				<br />
				<br /> Imagem Detalhes: <input type="file" name="imageDetalhes" /><br>
				<br />
				<br /> Título da Palestra: <input type="text" name="titulo" /><br> <br />Local
				da Palestra: <input type="text" name="local" /><br>
				<br /> <input type="submit" name="submit" value="Salvar" /><br>
			</form>
		</div>
	</div>

</body>

</html>
