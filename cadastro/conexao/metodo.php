<?php
require 'conexao/Base.php';

class Metodo extends Base
{

    public function buscarCardsPorAno($ano)
    {
        try {
            $sql = $this->conexao->prepare("select * from Tbl_Palestra as palestra inner join tbl_palestrante as palestrante on palestra.idpalestrante=palestrante.id where date_part('year',palestra.dataPalestra)=(?)");
            $sql->execute($ano);
            $dados = $sql->fetchAll();
            return $dados;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    public function salvarPalestrante($dados)
    {
        try {
            $sql = $this->conexao->prepare("insert into tbl_palestrante(nomepalestrante, biografia, nomeimagempalestrante, imagempalestrante) values (?, ?, ?, lo_import(?))");
            $sql->execute($dados);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function salvarPalestra($dados)
    {
        try {
            $sql = $this->conexao->prepare("insert into tbl_palestra(idpalestrante, descricao, duracao, entrada, datapalestra, imagecard, nomeimagecard, imagedetalhe, nomeimagedetalhe, titulo, localpalestra) values (?, ?, ?, ?, ?, lo_import(?), ?, lo_import(?), ?, ?, ?)");
            $sql->execute($dados);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function buscarPalestrante($id)
    {
        try {
            $sql = $this->conexao->prepare("select nomepalestrante, biografia, imagemPalestrante from tbl_palestrante where id = ?");
            // $sql = $this->conexao->prepare("select nomepalestrante, biografia, lo_export(imagemPalestrante, 'C:/imagens/' ||nomeimagemPalestrante) from tbl_palestrante where id = ?");
            $sql->execute($id);
            $dados = $sql->fetchAll();
            return $dados;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

?>