<?php
require 'conexao/Metodo.php';

$metodo = new Metodo();

if (isset($_POST['submit'])) {
    $nome = $_POST['nome'];
    $biblio = $_POST['biblio'];
    $nomeImage = pg_escape_string($_FILES["image"]["name"]);
    $imageData = $_FILES['image']["tmp_name"];
    $dados = array(
        $nome,
        $biblio,
        $nomeImage,
        $imageData
    );
    $retorno = $metodo->salvarPalestrante($dados);
    if ($retorno) {
        echo '<script>window.location.href = "index.php";</script>';
    } else {
        echo "Fracasso!";
    }
}

?>