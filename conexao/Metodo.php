<?php

require 'Base.php';

class Metodo extends Base
{

    public function buscarCardsPorAno($ano)
    {
        try {
            $sql = $this->conexao->prepare("select palestra.id, palestra.idpalestrante, palestra.descricao, palestra.duracao, palestra.entrada, palestra.datapalestra, lo_export(palestra.imagecard,'C:/xampp/htdocs/semana_academica/img/'||palestra.nomeimagecard), palestra.nomeimagecard, lo_export(palestra.imagedetalhe,'C:/xampp/htdocs/semana_academica/img/'||palestra.nomeimagedetalhe), palestra.nomeimagedetalhe, palestra.titulo, palestra.subtitulo, palestra.localpalestra, palestrante.id, palestrante.biografia, palestrante.nomepalestrante, lo_export(palestrante.imagempalestrante,'C:/xampp/htdocs/semana_academica/img/'||palestrante.nomeimagempalestrante), palestrante.nomeimagempalestrante from Tbl_Palestra as palestra inner join tbl_palestrante as palestrante on palestra.idpalestrante=palestrante.id where date_part('year',palestra.dataPalestra)=(?)");
            $sql->execute($ano);
            $dados = $sql->fetchAll();
            return $dados;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

    public function buscarPalestraPorId($id)
    {
        try {
            $sql = $this->conexao->prepare("select * from Tbl_Palestra as palestra inner join tbl_palestrante as palestrante on palestra.idpalestrante=palestrante.id where palestra.id = ?");
            $sql->execute($id);
            $dados = $sql->fetch();
            return $dados;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }

}

?>