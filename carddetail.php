<?php
    require 'conexao/Metodo.php';

    $metodo = new Metodo();
    $ano = array( $_GET['ano']);
    $id = array( $_GET['idPalestra']);
    $dados = $metodo->buscarPalestraPorId($id);
?>
<!doctype html>
<html lang="pt_br">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="https://www.unoesc.edu.br/favicon.ico" type="image/png">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="vendors/linericon/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
        <link rel="stylesheet" href="vendors/animate-css/animate.css">
        <link rel="stylesheet" href="vendors/jquery-ui/jquery-ui.css">
        <!-- main css -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="./css/_navbar.css" >

        <title>Semana Acadêmica <?php echo $ano[0] ?></title>
    </head>
    <body>
        <?php include('navbar.php'); ?>
        <!--================Blog Area =================-->
        <section class="blog_area p_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar">
                            <aside class="single_sidebar_widget author_widget">
                                <img class="author_img rounded-circle"  height="200px" width="200px"  src="img/<?php echo $dados['nomeimagempalestrante']; ?>" alt="">
                                <h4><?php echo $dados['nomepalestrante']; ?></h4>
                                <div class="social_icon">
                                    <a href="<?php echo $dados['facebook']; ?>"><i class="fa fa-facebook"></i></a>
                                    <a href="<?php echo $dados['twitter']; ?>"><i class="fa fa-twitter"></i></a>
                                </div>
                                <p> <?php echo $dados['biografia'];?> </p>
                                <div class="br"></div>
                            </aside>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="blog_left_sidebar">
                            <article class="blog_style1">
                            	<div class="blog_img">
                            		<img class="img-fluid" src="img/<?php echo $dados['nomeimagedetalhe']; ?>" alt="">
                            	</div>
                            	<div class="blog_text">
									<div class="blog_text_inner">
										<a href="single-blog.html"><h4><?php echo $dados['titulo'];?></h4></a>
										<p><?php echo $dados['descricao'];?></p>
										<div class="date">
											<a><i class="fa fa-calendar" aria-hidden="true"></i> Data da palestra:
                                                <?php echo date("d/m/Y", strtotime($dados['datapalestra'])) ?> </a>
                                        </div>
                                        <div class="date">
											<a><i class="fa fa-clock-o" aria-hidden="true"></i> Hora de ínicio:
                                                <?php echo date("H:i", strtotime($dados['datapalestra'])) ?> </a>
                                        </div>
                                        <div class="date">
                                            <a><i class="fa fa-hourglass-end" aria-hidden="true"></i> Duração:
                                                <?php echo $dados['duracao']; ?>h </a>	
                                        </div>
                                        <div class="date">
                                            <a><i class="fa fa-money" aria-hidden="true"></i> Entrada:
                                                <?php echo $dados['entrada'] != 0 ? $dados['entrada'] : 'Grátis' ?> </a>	
                                        </div>
                                        <div class="date">
                                            <a><i class="fa fa-map-marker" aria-hidden="true"></i> Local:
                                                <?php echo $dados['localpalestra'] ?> </a>	
                                        </div>
									</div>
                            	</div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
        <?php
            include 'footer.html';
        ?>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/stellar.js"></script>
        <script src="vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="vendors/isotope/isotope-min.js"></script>
        <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="vendors/jquery-ui/jquery-ui.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/theme.js"></script>
    </body>
</html>