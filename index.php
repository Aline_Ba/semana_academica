<?php
    require 'conexao/Metodo.php';

    $metodo = new Metodo();
    $ano = array(date('Y'));

    if(isset($_GET['ano'])){
      $ano = array($_GET['ano']);
    }

    $dados = $metodo->buscarCardsPorAno($ano);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="https://www.unoesc.edu.br/favicon.ico" type="image/png">
  <link rel="stylesheet" href="" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/_cards.css" >
    <link rel="stylesheet" href="./css/_navbar.css" >
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="vendors/linericon/style.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script type="text/javascript" src="js/navbar.js"></script>
    <title>Semana Acadêmica <?php echo $ano[0] ?></title>
</head>
<body>
<?php include('navbar.php'); ?>
<div class="titulo">
</div>
<div class="corpo">
<?php foreach($dados as $palestra): ?>
  <div class="blog-card" >
    <div class="meta">
      <div class="photo" style="background-image: url(img/<?php echo $palestra['nomeimagecard']?>);"></div>
      <ul class="details">
        <li class="author"><?php echo $palestra['nomepalestrante']?></a></li>
        <li class="date"><?php echo date("d/m/Y H:i", strtotime($palestra['datapalestra']))?></li>
      </ul>
    </div>
    <div class="description">
      <h1><?php echo $palestra['titulo']?></h1>
      <h2><?php echo $palestra['subtitulo']?></h2>
      <p> <?php echo $palestra['descricao']?></p>
      <p class="read-more">
        <a href="/semana_academica/carddetail.php?idPalestra=<?php echo $palestra[0]?>&ano=<?php echo $ano[0]?>">Detalhes</a>
      </p>
    </div>
  </div>
  </div> 
  <div>
  <?php endforeach; ?>
</div>
  
</body>
<?php include 'footer.html';?> 
</html>
