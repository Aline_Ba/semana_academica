<?php
    $ano = array(date('Y'));

    if(isset($_GET['ano'])){
        $ano = array($_GET['ano']);
    }
?>
<section class="navigation">
  <div class="nav-container">
    <div class="brand">
      <a href="/semana_academica/">Semana Acadêmica <?php echo $ano[0] ?></a>
    </div>
    <nav>
      <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
      <ul class="nav-list">
        <li>
          <a href="/semana_academica/?ano=2019">2019</a>
        </li>
        <li>
        <a href="/semana_academica/?ano=2017">2017</a>
        </li>
        <li>
        <a href="/semana_academica/?ano=2016">2016</a>
        </li>
        <li>
        <a href="/semana_academica/?ano=2015">2015</a>
        </li>
        <li>
        <a href="/semana_academica/?ano=2014">2014</a>
        </li>
      </ul>
    </nav>
  </div>
</section>